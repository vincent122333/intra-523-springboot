package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long>{
	
	public List<Personne> findPersonneByNom(String nom);
}
