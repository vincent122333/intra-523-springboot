package ca.claurendeau.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Personne;
import ca.claurendeau.repository.PersonneRepository;

@Service
public class serviceIntra {
	
	Logger logger = LoggerFactory.getLogger(serviceIntra.class);
	
	@Autowired
	private PersonneService personneService;
	
	@Autowired
	private PersonneRepository personneRepository;
	
	public void createInitialPersonnes() {
		personneService.savePersonne(new Personne("Georges", "georges@hotmail.ca"));
		personneService.savePersonne(new Personne("Julie", "julie@hotmail.ca"));
		personneService.savePersonne(new Personne("Paul", "paul@gmail.ca"));
	}
	
	public Personne findByNom(String nom) {
		List<Personne> personnes = personneRepository.findPersonneByNom(nom);
		return personnes.get(0);
	}

}
