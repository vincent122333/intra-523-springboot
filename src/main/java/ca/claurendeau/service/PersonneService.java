package ca.claurendeau.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Personne;
import ca.claurendeau.repository.PersonneRepository;

@Service
public class PersonneService {
	
	@Autowired
	private PersonneRepository personneRepository;
	
	public void savePersonne(Personne personne) {
		personneRepository.save(personne);
	}
	
	public Personne findByNom(String nom) {
		List<Personne> personnes = personneRepository.findPersonneByNom(nom);
		return personnes.get(0);
	}
}
