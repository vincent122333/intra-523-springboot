package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ca.claurendeau.domaine.Message;
import ca.claurendeau.domaine.Personne;
import ca.claurendeau.service.*;

@RestController
public class ReactControlleur {
	
	@Autowired
	private serviceIntra serviceIntra;
    
    @CrossOrigin
    @GetMapping("/donneMoiLaPersonne/{personneNom}")
    public @ResponseBody Personne donneMoiLaPersonne(@PathVariable String personneNom) {
		return serviceIntra.findByNom(personneNom);
	}
    
    /*@CrossOrigin    
    @GetMapping("VOTRE_ENDPOINT_ICI")
    // VOTRE IMPLANTATION ICI*/
}
